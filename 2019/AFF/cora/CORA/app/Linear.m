%System
spaceEx_file = ' \ ';
spaceex2cora(spaceEx_file);
%Settings
options.tstart = 0;
options.tfinal = 1;
options.time_step = 0.01;
options.zOrder = 50;
options.taylor_terms = 20;
options.reduction_technique = 'redistribute';
options.linear_algorithm = 1;
options.R0 = interval(evalin('base', 'B'), eye(199));
options.U = interval(evalin('base', 'B'), eye(3));
options.uTrans = evalin('base', 'C');
options.uTransVec = evalin('base', 'ans');

