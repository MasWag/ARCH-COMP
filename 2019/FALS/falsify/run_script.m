% (C) 2019 National Institute of Advanced Industrial Science and Technology
% (AIST)


% Initialization
%%%%%%%%%%%%%%%%
initialization

% ARCH2014 Benchmark
%%%%%%%%%%%%%%%%%%%%

%disp('running steamcondenser benchmark');
%run_steamcondense_script
%disp('running neural benchmark');
%run_nn_script
%disp('running ptc benchmark');
%run_ptc_script
disp('running chasing cars benchmark');
run_chasing_cars_script
disp('running transmission benchmark');
run_transmission_script
%disp('running WT benchnmark');
run_wt_script

% Output
%%%%%%%%%%%%%%%%%%%

format_table