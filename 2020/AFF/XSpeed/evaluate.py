import os
import string
import re
from subprocess import Popen, PIPE

# examples organized with input/output file/string results for parsing runtime results
example=[]
example.append(['Heat3D','HEAT01', 'HEAT01', '--time-step 0.02 --time-horizon 40 -v t,x_62'])

example.append(['ISS', 'ISSF01', 'ISSF01-ISS01', '--time-step 0.0005 --time-horizon 20 -v t,y3'])
example.append(['ISS', 'ISSF01', 'ISSF01-ISU01', '--time-step 0.0005 --time-horizon 20 -v t,y3'])
example.append(['ISS', 'ISSC01', 'ISSC01-ISS02', '--time-step 0.01 --time-horizon 20 -v t,y3'])
example.append(['ISS', 'ISSC01', 'ISSC01-ISU02', '--time-step 0.01 --time-horizon 20 -v t,y3'])

example.append(['Rendezvous', 'SRNA01', 'SRNA01-SR02', '--time-step 0.01 --time-horizon 240 --depth 2 -v x,z'])
example.append(['Rendezvous', 'SRA01', 'SRA01-SR02', '--directions 2 --time-step 0.01 --time-horizon 240 --depth 3 -v x,z'])

example.append(['Building', 'BLDC01', 'BLDC01-BDS01', '--time-step 0.005 --time-horizon 20 -v t,x25'])
example.append(['Building', 'BLDC01', 'BLDC01-BDU01', '--time-step 0.005 --time-horizon 20 -v t,x25'])
example.append(['Building', 'BLDC01', 'BLDC01-BDU02', '--time-step 0.005 --time-horizon 20 -v t,x25'])
example.append(['Building', 'BLDF01', 'BLDF01-BDS01', '--time-step 0.005 --time-horizon 20 -v t,x25'])
example.append(['Building', 'BLDF01', 'BLDF01-BDU01', '--time-step 0.005 --time-horizon 20 -v t,x25'])
example.append(['Building', 'BLDF01', 'BLDF01-BDU02', '--time-step 0.005 --time-horizon 20 -v t,x25'])

example.append(['Platoon', 'PLAD01-BND', 'PLAD01-BND42', '--time-step 0.01 --time-horizon 5 --depth 3 -v T,e1'])
example.append(['Platoon', 'PLAD01-BND', 'PLAD01-BND30', '--directions 2 --time-step 0.001 --time-horizon 5 --depth 3 -v T,e1'])
example.append(['Platoon', 'PLAN01-UNB', 'PLAN01-UNB50', '--time-step 0.01 --time-horizon 20 --depth 10 -v T,e1'])

example.append(['Gearbox', 'GRBX01', 'GRBX01-MES01', '--directions 2 --time-step 0.0001 --time-horizon 5 --depth 5 -v px,py'])
example.append(['Gearbox', 'GRBX01', 'GRBX02-MES01', '--directions 2 --time-step 0.0001 --time-horizon 5 --depth 5 -v px,py'])

# results in ./result
if not os.path.isdir('result'):
    os.mkdir('result')
    # populate the results.csv file	
with open("./result/results.csv","w") as f:
   for i in range(0,len(example)):			
       string_cmd = 'docker run xspeed -m ./AFF/' + example[i][0] + '/' + example[i][1] + '.xml -c ./AFF/' + example[i][0] + '/' + example[i][2] + '.cfg ' + example[i][3]
       print('\nrunning '+ example[i][2])
       p = Popen(string_cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE, shell=True)
       output = str(p.communicate())
       safety = "Model is SAFE"
       if output.find(safety)==-1:
        safety_res = 0
       else:
        safety_res = 1
       time_str = "User Time  (in Seconds) ="
       r_idx = output.find(time_str)
       r_idx_time_start = int(r_idx)+len(time_str)+1
       r_idx_time_end = int(r_idx)+len(time_str)+8
       
       if r_idx >= 0:
        output_time = output[r_idx_time_start:r_idx_time_end];
        # remove non-numeric characters (except .)
        output_time = re.sub("[^\d\.]", "", output_time)
        f.write('xspeed, '+example[i][1]+', '+ example[i][2]+ ', '+ str(safety_res)+', '+output_time+'\n')
f.close()
os.chdir('..')	
print("results.csv file of XSpeed created successfully :)\n")
