function [val,x] = norm(E)
% norm - compute the maximum Euclidean norm of an ellipsoid, measured from
% the center
%
% Syntax:  
%    [val,x] = norm(E)
%
% Inputs:
%    A - numerical matrix
%    E - Ellipsoid object 
%
% Outputs:
%    val - value of the maximum Euclidean norm (measured from the center)
%    x   - point x (one of the two) which attains val
%
% Example: 
%    E = ellipsoid.generateRandom(false,2);
%    [val,x] = norm(E);
%    figure
%    hold on;
%    plot(E);
%    plot(x(1),x(2),'xr');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Victor Gassmann
% Written:      20-November-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
n = E.dim;
[U,D] = eig(E.Q);
[lmax,ind] = max(diag(D));
I = eye(n);
y = sqrt(lmax)*I(:,ind);
val = sqrt(lmax);
x = U*y+E.q;
%------------- END OF CODE --------------