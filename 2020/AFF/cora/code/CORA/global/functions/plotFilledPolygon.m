function han = plotFilledPolygon(V,varargin)
% plotFilledPolygon - plot a filled polygon defined by its vertices
%
% Syntax:  
%    han = plotFilledPolygon(V,plotOptions)
%
% Inputs:
%    V - matrix storing the polygon vertices
%    plotOptions - plot settings specified as name-value pairs
%
% Outputs:
%    han - handle of graphics object
%
% Example: 
%    zono = zonotope.generateRandom(2);
%    V = vertices(zono);
%
%    plotFilledPolygon(V,'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plotPolygon

% Author:       Niklas Kochdumper
% Written:      05-May-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% check if vertex array is not empty
if ~isempty(V)

    % compute convex hull for more than two vertices
    if size(V,2)>2

        try
            ind = convhull(V(1,:),V(2,:));
            V = V(:,ind);
        catch
            error('Plotting the set failed');
        end
    end

    % plot the constrained zonotope
    han = fill(V(1,:), V(2,:), varargin{:});
end

%------------- END OF CODE --------------