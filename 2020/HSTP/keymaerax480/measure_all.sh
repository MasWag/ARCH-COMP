#!/bin/sh

docker build -t keymaerax480:1.0 .
docker create -it -v $PWD/Licensing:/root/.WolframEngine/Licensing --name kyx480 keymaerax480:1.0 bash
docker start kyx480
docker exec -it kyx480 bash -c "mkdir /root/.keymaerax"
docker cp ./keymaerax.wolframengine.conf kyx480:/root/.keymaerax/keymaerax.conf
docker exec -it -w /root/arch2020/ kyx480 java -jar keymaerax_480.jar -setup
docker exec -it -w /root/arch2020 kyx480 bash "./runKeYmaeraX480ScriptedBenchmarks"
docker exec -w /root/arch2020 kyx480 bash -c "mkdir results; mv *.csv results"
docker cp kyx480:/root/arch2020/results .
docker rm -f kyx480
