function result = containsPoint(Z1,p,varargin)
% containsPoint - determines if the point p is inside the zonotope Z1
%
% Syntax:  
%    result = containsPoint(Z1,p)
%    result = containsPoint(Z1,p,tolerance)
%
% Inputs:
%    Z1 - zonotope object
%    p - point specified as a vector
%    tolerance - numerical tolerance up to which the point is allowed to 
%                outside the zonotope
%
% Outputs:
%    result - 1/0 if point is inside the zonotope or not
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author:       Niklas Kochdumper
% Written:      30-January-2018 
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% parse input arguments
if nargin == 3
   tolerance = varargin{1};
else
   tolerance = 0;
end

% generate halfspace representation if empty
if isempty(Z1.halfspace)
    Z1 = halfspace(Z1);
end

%simple test: Is point inside the zonotope?
N = length(Z1.halfspace.K);
inequality = (Z1.halfspace.H*p - Z1.halfspace.K <= tolerance * ones(N,1));

result = (all(inequality));

%------------- END OF CODE --------------
