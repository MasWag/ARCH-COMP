function f = coupledVanDerPol1(x,u)

    mu = 1;
    
    f(1,1) = x(2);
    f(2,1) = mu*(1-x(1)^2)*x(2) - 2*x(1) + x(3);
    f(3,1) = x(4);
    f(4,1) = mu*(1-x(3)^2)*x(4) - 2*x(3) + x(1);
end