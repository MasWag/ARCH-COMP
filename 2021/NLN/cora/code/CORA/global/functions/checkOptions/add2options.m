function add2options(name,status,checkfuncs,varargin)
% function add2options(name,status,checkfuncs,errmsgs,varargin)

global fullOptionsList;

if isempty(varargin)
    fullOptionsList = add2list(fullOptionsList,name,status,checkfuncs);
%     fullOptionsList = add2list(fullOptionsList,name,status,checkfuncs,errmsgs);
else
    fullOptionsList = add2list(fullOptionsList,name,status,checkfuncs,varargin{1});
%     fullOptionsList = add2list(fullOptionsList,name,status,checkfuncs,errmsgs,varargin{1});
end

end