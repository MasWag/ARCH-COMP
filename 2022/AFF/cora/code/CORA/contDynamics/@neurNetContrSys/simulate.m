function [t,x,ind,y] = simulate(obj,params,varargin)
% simulate - simulates a nonlinear discrete-time system
%
% Syntax:  
%    [t,x] = simulate(obj,params)
%    [t,x,ind] = simulate(obj,params,options)
%    [t,x,ind,y] = simulate(obj,params,options)
%
% Inputs:
%    obj - neurNetContrSys object
%    params - struct containing the parameters for the simulation
%       .tStart: initial time
%       .tFinal: final time
%       .x0: initial point
%       .u: piecewise constant input signal u(t) specified as a matrix
%           for which the number of rows is identical to the number of
%           system inputs
%    options - ODE45 options (for hybrid systems)
%
% Outputs:
%    t - time vector
%    x - state vector
%    ind - returns the event which has been detected
%    y - output vector
%
% Example: 
%    f = @(x,u) [x(1) + u(1);x(2) + u(2)*cos(x(1));x(3) + u(2)*sin(x(1))];
%    dt = 0.25;
%    sys = nonlinearSysDT(f,dt);
%    
%    params.x0 = [0;0;0];
%    params.tFinal = 1;
%    params.u = [0.02 0.02 0.02 0.02; 2 3 4 5;];
%
%    [t,x] = simulate(sys,params);
%
%    plot(x(:,2),x(:,3),'.k','MarkerSize',20);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: neurNetContrSys

% Author:       Niklas Kochdumper
% Written:      13-December-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% parse input arguments
if nargout == 3
    ind = [];
end
if nargout == 4
    warning("Output trajectories not supported for class nonlinearSysDT!");
	y = [];
end

isOpt = false;
if nargin >= 3 && ~isempty(varargin{1})
	options = varargin{1}; 
    isOpt = true;
end

if ~isfield(params,'tStart')
	params.tStart = 0; 
end

time = (params.tStart:obj.dt:params.tFinal)';
if time(end) ~= params.tFinal
   time = [time;params.tFinal]; 
end

% consider changing inputs
if size(params.u,2) ~= 1
    tu = linspace(params.tStart,params.tFinal,size(params.u,2));
else
    tu = time;
    params.u = params.u*ones(1,length(time)-1);
end

% simulate the system
params_ = params;
t = []; x = []; ind = [];
x0 = params.x0; cnt = 1;

for i = 1:length(time)-1
    
    % compute control input for the current state
    u = evaluate(obj.nn,x0);
    x0 = [x0;u];
    
    % get number of disturbance changes duing sampling time
    index = find(tu(cnt:end) < time(i+1));
    w = params.u(cnt:cnt+index(end)-1); 
    
    % loop over the number of disturbance changes
    for j = 1:size(w,2)

        params_.u = params.u(:,j);
        params_.x0 = x0;
        params_.w = zeros(length(x0),1);             % for linear systems
        
        if isfield(params,'timeStep')
            tSpan = tu(cnt+j-1):params.timeStep:tu(cnt+j);
            if abs(tSpan(end)-tu(cnt+j)) > 1e-10
               tSpan = [tSpan,tu(cnt+j)]; 
            end
        else
            tSpan = [tu(cnt+j-1),tu(cnt+j)];
        end

        % simulate using MATLABs ode45 function
        try
            if isOpt
                [t_,x_,~,~,ind] = ode45(getfcn(obj.sys,params_),tSpan, ...
                                                               x0,options);
            else
                [t_,x_,~,~,ind] = ode45(getfcn(obj.sys,params_),tSpan,x0);
            end
        catch
            if isOpt
                [t_,x_] = ode45(getfcn(obj.sys,params_),tSpan,x0,options);
            else
                [t_,x_] = ode45(getfcn(obj.sys,params_),tSpan,x0);
            end
        end

        % store the results
        x = [x; x_(:,1:obj.dim)]; 
        t = [t; t_];
        x0 = x(end,1:obj.dim)';
        cnt = cnt + index(end);

        if ~isempty(ind)
            return; 
        end
    end
end

%------------- END OF CODE --------------