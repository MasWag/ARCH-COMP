function [count,out] = numberOfInputs(f,varargin)
% numberOfInputs - computes the number of inputs of a function handle
%
% Syntax:  
%    [count,out] = numberOfInputs(f)
%    [count,out] = numberOfInputs(f,inpArgs)
%
% Inputs:
%    f - function handle 
%    inpArgs - number of input arguments for the function (max. 26)
%
% Outputs:
%    count - vector storing the length of each input argument
%    out - output dimension of the function handle
%
% Example:
%    f = @(x,u) [x(1)*x(5)^2; sin(x(3)) + u(2)];
%    numberOfInputs(f)
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: nonlinearSys

% Author:       Victor Gassmann
% Written:      11-September-2020
% Last update:  17-June-2022 (MW, extend fast way to case where given
%                                 inpArgs exceeds nargin to f)
% Last revision:---

%------------- BEGIN CODE --------------

% parse input arguments
inpArgs = setDefaultValues({{nargin(f)}},varargin{:});

% too many input arguments to dynamic function
if inpArgs > 26
    throw(CORAerror('CORA:wrongValue','second',...
        'integer value between 1 and 26.'));
end

% try fast way to determine number of inputs (does not work if
% statements like "length(x)" occur in the dynamic function
try
    % number of input arguments to function handle
    narginf = nargin(f);
    % create symbolic variables (length 100 for each input)
    x = sym('x',[100,narginf]);
    xc = num2cell(x,1);
    
    % evaluate function
    fsym = f(xc{1:narginf});
    
    % output dimension of f
    out = length(fsym);
    % used variables from [100,inpArgs] symbolic variables
    vars = symvar(fsym);
    % logical indices for used variables
    mask = ismember(x,vars);
    % required dimension of each inpArgs for evaluation of f
    count = zeros(inpArgs,1);
    for i=1:narginf
        % last 'true' value in logical indices is required dimension
        tmp = find(mask(:,i),1,'last');
        if isempty(tmp)
            tmp = 0;
        end
        count(i) = tmp;
    end
    return;
end

% upper bound
bound = 100000;
maxVal = 10;
s = 'abcdefghijklmnopqrstuvwxyz';
input = cell(inpArgs,1);

while true
   
    found = 0;
    comb = combinator(maxVal,inpArgs);
    
    if size(comb,1) > bound
       error('Could not determine length of input arguments!'); 
    end
    
    for i = 1:size(comb,1)
        for j = 1:inpArgs
            input{j} = sym(s(j),[comb(i,j),1]);
        end

        % pass symbolic inputs to function handle
        try
            output = f(input{:}); 
            found = 1;
            break;
        catch
            continue;
        end
    end
    
    if found
        break; 
    else
        maxVal = 2*maxVal;
    end
end


% get all symbolic variables that are contained in the output
vars = symvar(output);

% get required length for each input argument
count = zeros(inpArgs,1);

for i = 1:length(vars)
    
    % split variable into string and number
    temp = char(vars(i));
    str = temp(1);
    num = str2double(temp(2:end));
    
    % update counter
    ind = find(s == str);
    count(ind) = max(count(ind),num);
end

out = length(output);

%------------- END OF CODE --------------