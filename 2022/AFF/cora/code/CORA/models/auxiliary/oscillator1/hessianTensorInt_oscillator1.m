function Hf=hessianTensorInt_oscillator1(x,u)



 Hf{1} = interval(sparse(6,6),sparse(6,6));



 Hf{2} = interval(sparse(6,6),sparse(6,6));



 Hf{3} = interval(sparse(6,6),sparse(6,6));



 Hf{4} = interval(sparse(6,6),sparse(6,6));



 Hf{5} = interval(sparse(6,6),sparse(6,6));

Hf{5}(3,2) = 200*x(3)^3*x(5)^2;
Hf{5}(5,2) = 100*x(3)^4*x(5);
Hf{5}(2,3) = 200*x(3)^3*x(5)^2;
Hf{5}(3,3) = 60*x(3)^2*x(5)^2*(10*x(2) - (3*x(3))/2) - 60*x(3)^3*x(5)^2;
Hf{5}(5,3) = 40*x(3)^3*x(5)*(10*x(2) - (3*x(3))/2) - 15*x(3)^4*x(5);
Hf{5}(2,5) = 100*x(3)^4*x(5);
Hf{5}(3,5) = 40*x(3)^3*x(5)*(10*x(2) - (3*x(3))/2) - 15*x(3)^4*x(5);
Hf{5}(5,5) = 10*x(3)^4*(10*x(2) - (3*x(3))/2);
