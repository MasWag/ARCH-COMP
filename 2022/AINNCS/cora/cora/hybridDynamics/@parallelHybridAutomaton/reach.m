function [R,res] = reach(pHA,params,options,varargin)
% reach - computes the reachable set for a parallel hybrid automaton
%
% Syntax:  
%    R = reach(pHA,params,options)
%    [R,res] = reach(pHA,params,options,spec,spec_mapping)
%
% Inputs:
%    pHA - parallelHybridAutomaton object
%    params - parameter defining the reachability problem
%    options - options for the computation of reachable sets
%    spec - object of class specification 
%    spec_mapping - cell array specifying which specifications are relevant
%                   to which locations of which subcomponent
%                   e.g.: spec_mapping =  {{[1,2]; [1]}, {[2]; [3]}} means
%                     subcomponent 1: specs 1,2 for location 1, spec 1 for
%                     location 2
%                     subcomponent 2: spec 2 for location 1, spec 3 for
%                     location 2
% Outputs:
%    R - reachSet object storing the reachable set
%    res - true/false whether specifications are satisfied
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: parallelHybridAutomaton

% Author:       Niklas Kochdumper
% Written:      04-July-2018 
% Last update:  14-June-2020
%               13-October-2021 (MP, implemented location-specific
%                                specifications)
%               03-March-2022 (MP, implemented synchronization labels)
% Last revision:---

%------------- BEGIN CODE --------------

    res = true;
    
    % new options preprocessing
    options = validateOptions(pHA,mfilename,params,options);

    % preprocessing of specifications
    options.specification = [];
    spec = [];
    spec_mapping = {};
    
    if nargin >= 4
       spec = varargin{1};
       if nargin >= 5
           spec_mapping = varargin{2};
       end
    end
    
    % initialize reachable set
    R = [];

    % initialize queue
    list{1}.set = options.R0;
    list{1}.loc = options.startLoc;
    list{1}.time = interval(options.tStart);
    list{1}.parent = 0;

    % display information on command window
    if options.verbose
        disp("Start analysis...");
        disp("  pre-process synchronization labels...");
    end

    % initialize tracker
    tracker = struct('switchingTime',interval(),...
        'locID',double.empty(0,length(options.startLoc)),...
        'transition',double.empty(0,length(options.startLoc)),...
        'syncLabel','');

    % create list of label occurences to check whether all labeled
    % transitions are enabled at the same time
    allLabels = labelOccurrences(pHA);

    % number of iterations in the main loop
    k = 0;
    
    % loop until the queue is empty or a specification is violated
    while ~isempty(list) && res

        % increment number of iterations
        k = k + 1;

        % update tracking
        tracker(k,1).switchingTime = list{1}.time;
        tracker(k,1).locID = list{1}.loc;

        % get locations for each hybrid automaton, initial set for 
        % automaton product, start time, and parent branch for reachable
        % set computation of first element in the queue
        locID = list{1}.loc;
        R0 = list{1}.set;
        tStart = list{1}.time;
        parent = list{1}.parent;

        % filter the specifications for the ones relevant to the local
        % automaton product
        if ~isempty(spec_mapping)
            options.specification = filterSpecifications(locID,spec,spec_mapping);
        else
            options.specification = spec;
        end
        
        % compute input set for the constructed location
        options.U = mergeInputSet(locID,options.Uloc,options.inputCompMap);
        
        % check for immediate transitions
        [list,tracker,restart] = immediateTransition(pHA,list,locID,allLabels,...
            R0,options.U,tracker,options.verbose);
        % check for livelock
        if checkLivelock(tracker); break; end
        % restart if immediate transition has occurred
        if restart; continue; end

        % construct new location with local Automaton Product
        if options.verbose
            disp("  compute location product of locations [" + ...
               strjoin(string(locID),',') + "]...");
        end
        locObj = locationProduct(pHA,locID,allLabels);

        % compute the reachable set within the constructed location
        if options.verbose
            disp("  compute reachable set in locations [" + ...
               strjoin(string(locID),',') + "]...");
        end
        [Rtemp,Rjump,res] = reach(locObj,R0,tStart,options);

        % remove current element from the queue
        list = list(2:end);

        % add the new sets to the queue
        for i = 1:length(Rjump)
            Rjump{i}.parent = Rjump{i}.parent + length(R);
        end
        list = [list; Rjump];

        % display transitions on command window
        if options.verbose && isscalar(list)
            % multiple successor locations currently not supported...
            disp("  transition: locations [" + strjoin(string(locID),",") + ...
                "] -> locations [" + strjoin(string(list{1}.loc),",") + ...
                "]... (time: " + string(list{1}.time) + ")");
        end

        % store the computed reachable set
        for i = 1:size(Rtemp,1)
            temp = reachSet(Rtemp.timePoint,Rtemp.timeInterval,...
                                Rtemp.parent,locID);
            R = add(R,temp,parent);
        end
    end
end
    
    
% Auxiliary Functions -----------------------------------------------------

function U = mergeInputSet(loc,Uloc,inputCompMap)
% compute the joint input set for the location generated via the automaton
% product of all individual hybrid automata

    % loop over all used components
    numInp = length(inputCompMap);
    comp = unique(inputCompMap);
    
    for i=1:length(comp)
        
        % find indices of component in input set
        ind = find(inputCompMap == comp(i));
        
        % project input set of location from individual hybrid automaton
        % to higher-dimensional space of automaton product
        Utemp = projectHighDim(Uloc{comp(i)}{loc(comp(i))},numInp,ind);
        
        % add to overall input set
        if i == 1
            U = Utemp; 
        else
            U = U + Utemp; 
        end
    end

end

function activeSpecs = filterSpecifications(loc,spec,spec_mapping)
% filter the specifications for the ones relevant to the local
% automaton product: if a specific subcomponent is within a
% location where a specification is relevant, the specification is
% analyzed in the next reachability step
    
    % start with empty list, collect indices of relevant specifications
    activeSpecs_indices = [];
    
    for i=1:length(loc)
        activeSpecs_indices = [activeSpecs_indices spec_mapping{i}{loc(i)}];
    end
    
    % filter out specifications which are activated more than once
    activeSpecs_indices = unique(activeSpecs_indices);
    
    % return specification object containing only "active" specifications
    activeSpecs = spec(activeSpecs_indices);

end

%------------- END OF CODE --------------
