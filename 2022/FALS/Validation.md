# Validation of Experimental Results with FalStar

Contact: Gidon Ernst

Please refer to [Participation.md](Participation.md) for how to participate in the falsification track of ARCH-COMP 2022.
Note that there have been some changes since 2021.

FalStar accepts as input CSV files in the following format (see also [rfc4180](https://datatracker.ietf.org/doc/html/rfc4180))

- the first line is a header with column names, in any order
- all subsequent lines reflect individual experiments, with entries in the order of the respective header
- column separator is a comma `,`
- complex entries should be enclosed in double quotes `"` but need not to be,
  for example `robustness`, `yes` or `"yes"`, and `"□_[0.0, 20.0] (speed < 120.0)"`,
- numbers can specified in any format accepted by [`Double.parseDouble`](https://docs.oracle.com/javase/7/docs/api/java/lang/Double.html),
  and do not need quotes, e.g., `0`, or `1.2`
- column names that are interpreted are discussed below, additional columns are ignored
- no whitespace around entries is allowed even in the presence of double quotes, ok: `yes,1.0`, not ok: ` no , "-1.0"`
- please no blank lines (possibly not enfored by the parser)
- boolean values `yes`, `true`, `1` are recognized as true (case sensitive), everything else is considered to be false

Terminology
- *reported* refers to the data submitted by participants in the input file
- *computed* what FalStar determines on its own

## Example

**Warning**: this example does *not* properly sample the input signal as described below, to shorten the presentation here.
It does, however, include the main information to get you started.

    "system","property","simulations","time","robustness","falsified","input"
    "AT","AT1",10,2,55.086463580698,"no","[0.0 50.0 0.0; 10.0 0.0 325.0; 20.0 0.0 325.0]"
    "AFC","(AFC27 0.008)",4,12,-0.000327114731111,"yes","[0.0 0.0 1100.0; 5.5 45.825 1100.0; 11.0 61.1 1100.0; 16.5 61.1 1100.0; 22.0 45.825 1100.0; 27.5 0.0 1100.0; 33.0 61.1 1100.0; 38.5 61.1 1100.0; 44.0 30.55 1100.0; 49.5 38.1875 1100.0; 55.0 38.1875 1100.0]"
    "CC","CC1",8,9,-58.3266758840495,"yes","[0.0 0.5 1.0; 5.0 1.0 0.0; 10.0 1.0 0.0; 15.0 0.0 0.0; 20.0 1.0 0.0; 25.0 0.0 0.0; 30.0 0.0 0.0; 35.0 1.0 0.0; 40.0 0.5 1.0; 45.0 1.0 0.0; 50.0 0.0 0.0; 55.0 1.0 0.0; 60.0 0.0 0.0; 65.0 0.5 0.0; 70.0 0.5 0.5; 75.0 1.0 0.25; 80.0 1.0 0.5; 85.0 0.0 1.0; 90.0 0.625 0.0; 95.0 0.75 1.0; 100.0 0.75 1.0]"
    "NN","(NN 0.005 0.03)",10,4,0.033247279060779,"no","[0.0 1.5; 7.0 2.0; 17.5 2.5; 21.0 2.5]"
    "SC","SCa",10,1,0.206327753620442,"no","[0.0 4.0; 17.5 4.0; 35.0 4.0]"

See below for system and property identifiers related to the benchmarks of ARCH 2022.

For a conrete example, see some preliminary results from the ARIsTEO team

- [Reported Results](results/log_Aristeo_pchip_demo.csv)
- [Validation Log](results/ARIsTEO/validation-log.csv) with one line per data point from the reported results
- [Validation Report](results/ARIsTEO/validation-report.csv) with one line that aggregates per property + instance

You can re-run these experiments from the `validation` subfolder, provided Java >= 1.8 and some reasonable version of Matlab is installed:

    ./falstar-config.sh # once to set up the Matlab path
    ./falstar.sh validate-aristeo-demo.cfg

Note that this will *append* new entries to the files above, without overwriting the example results from validation.

## Columns

- `system` refers to a system model in terms of a fixed key (see below)
- `property` refers to a specific requirement in terms of a fixed key (see below)
- `instance` (optional): either 1 or 2 as described in the report
- `input` (optional): a time series of inputs will be passed directly to the Simulink model as `ExternalInput`
- `parameters` (required when `input` is given and model has initial conditions): a vector of values for the initial conditions, in the order shown below
- `falsified` (optional, used if `input` is given): whether running the reported `input` is expected fo falsify `property`
- `simulations` (optional): how many simultations were needed for falsification, aggregated in the output for `falsified` simulations only
- `simulation time` (optional, **new**): overall wall time spent inside the simulation engine (i.e. Simulink), *not* counting additional computations
- `total time` (optional, **new**): overall wall time to conduct the experiment number of simulations + intermediate computation
- `time` (optional): alternative name for `total_time` if that is not given
- `robustness` (optional, used if `input` is given): the numeric robustness value as by the quantitative semantics of STL/MTL formula (using difference, `min`, `max`)
- `stop time` (optional, used if `input` is given): time horizon for the validation, assigned to `StopTime` in Simulink, if not present it is taken from the last entry in `input`
- `output` (optional, used if `input` is given): expected output signal, formatted as time-series analogously to `input`

Alternative to `simulation time` and `total time` you might collect the fraction between 0 and 1 which the simulation time takes of the total time,
e.g. as `simulation time ratio` or similarly.

## A note on input signals

FalStar's validator will detect coarsely sampled inputs with control point spacing at least `1.0`
and upsample the input signal with `dt = 0.1`, using an interpolation analogous to *previous* in Matlab.

Moreover, the stopping time (i.e. length of the simulation) will be determined as follows
- If there is a column `stop time` it will be used
- Otherwise, if the signal is coarse (or if the input signal is empty, which should not happen),
  the stopping time is computed from the formula.
  This is needed because the last control point of a coarse signal may not reflect the stopping time correctly.
  This means, that for example for `AT1`, an `input` of `[0 100 0; 15 100 0]` is valid and will be converted to signal
  `[0.0 100 0; 0.1 100; ...; 15.0 100 0; 15.1 100 0; ...; 20 100 0]` where `20` is the longest useful simulation time
  (i.e., when the formula's truth is fully determined).
- Otherwise, the provided signal will be used without change and thus the stopping time is that of the last entry in the input signal

For best reproducability **please provede finely sampled input signals**, which have a sufficiently small time granularity.
This means, in particular, for piecewise constant signals, you might want to **interpolate** yourself.

S-TaLiRo for example uses this default setting in `staliro_options.m`, which is likely small enough for all purposes.

   Default option : SampTime = .05;

Input signals are passed verbatim to the `ExternalInput` argument of Matlab's `sim` function,
which in turn passes them in order to the `Input` ports in the Simulink model from the `models/FALS` folder.

The `Input` ports maye have (linear) interpolation enabled, which means that giving it a piecewise constant signal specified by control points
will *not* end up as an input with discontinuities but with ramps.
For example, Matlab will interpret `[0 0; 1 10]` as `[0.0 0.0; 0.1 1.0; 0.2; 2.0; ...]`.

## Results of Validation

FalStar then simulates the model using the reported `parameters`, `input`, `stopping time` if they are present,
and checkes whether `falsified`, `robustness`, and `output` can be reproduced, respectively, in case these are given.

- `formula`: a human readable representation of the formula corresponding to `property` for cross-checking
- `parameters valid` and `inputs valid`, if the respective values are given and are all within the bounds set for the benchmarks
- `falsified correct` if the *reported* `falsified` flag matches the *computed* one
- `robustness correct` if the sign of the *reported* `robustness` matches the *computed* one
- `robustness error`: the absolute diffference between *reported* and *computed* robustness
- `output error`: the maximal pointwise difference between *reported* and *computed* output per time point

## List of Properties used in ARCH 2022

Valid values for `system` with some comment on `parameters` and `inputs`.
If `parameters` is not mentioned (they are only relevant for `F16`), you can just leave out the column or set it to the empty vector `[]`.

- `AT`: automatic transmission
- `CC`: chasing cars
- `AFC_normal` and `AFC_power`: abstract fuel control (power train) with normal mode and power mode respectively
- `NN`: neural network controller
- `SC`: steam condenser
- `F16`: F16 GCAS altitude control
  - five dimensional `parameter` giving the initial altitude, airspeed, roll, pitch, yaw, in that order, corresponding to the MATLAB variables `altg`, `Vtg`, `phig`, `thetag`, `psig` used in `run_f16.m`, where `altg = 4040` for the standard benchmark and `Vtg = 540`

Valid values for `property` and the associated formulas (in human readable form), where the prefix has to match `system`.
Some formulas are parameterized, here the parentheses must be included, and you can change the parameter.

    AT1                 □_[0.0, 20.0] (speed < 120.0)
    AT2                 □_[0.0, 10.0] (RPM < 4750.0)
    AT51                □_[0.0, 30.0] (((gear ≠ 1.0) ∧ ◇_[0.001, 0.1] (gear = 1.0)) ⟹ ◇_[0.001, 0.1] □_[0.0, 2.5] (gear = 1.0))
    AT52                □_[0.0, 30.0] (((gear ≠ 2.0) ∧ ◇_[0.001, 0.1] (gear = 2.0)) ⟹ ◇_[0.001, 0.1] □_[0.0, 2.5] (gear = 2.0))
    AT53                □_[0.0, 30.0] (((gear ≠ 3.0) ∧ ◇_[0.001, 0.1] (gear = 3.0)) ⟹ ◇_[0.001, 0.1] □_[0.0, 2.5] (gear = 3.0))
    AT54                □_[0.0, 30.0] (((gear ≠ 4.0) ∧ ◇_[0.001, 0.1] (gear = 4.0)) ⟹ ◇_[0.001, 0.1] □_[0.0, 2.5] (gear = 4.0))
    AT6a                (□_[0.0, 30.0] (RPM < 3000.0) ⟹ □_[0.0, 4.0] (speed < 35.0))
    AT6b                (□_[0.0, 30.0] (RPM < 3000.0) ⟹ □_[0.0, 8.0] (speed < 50.0))
    AT6c                (□_[0.0, 30.0] (RPM < 3000.0) ⟹ □_[0.0, 20.0] (speed < 65.0))


    (AFC27 0.008)       □_[11.0, 50.0] ((((throttle < 8.8) ∧ ◇_[0.0, 0.05] (40.0 < throttle)) ∨ ((40.0 < throttle) ∧ ◇_[0.0, 0.05] (throttle < 8.8))) ⟹ □_[1.0, 5.0] (abs(mu) < 0.008))
    (AFC29 0.007)       □_[11.0, 50.0] (abs(mu) < 0.007)
    (AFC33 0.007)       □_[11.0, 50.0] (abs(mu) < 0.007)

    CC1                 □_[0.0, 100.0] ((y5 - y4) ≤ 40.0)
    CC2                 □_[0.0, 70.0] ◇_[0.0, 30.0] (15.0 ≤ (y5 - y4))
    CC3                 □_[0.0, 80.0] (□_[0.0, 20.0] ((y2 - y1) ≤ 20.0) ∨ ◇_[0.0, 20.0] ((y5 - y4) ≥ 40.0))
    CC4                 □_[0.0, 65.0] ◇_[0.0, 30.0] □_[0.0, 5.0] (8.0 ≤ (y5 - y4))
    CC5                 □_[0.0, 72.0] ◇_[0.0, 8.0] (□_[0.0, 5.0] (9.0 ≤ (y2 - y1)) ⟹ □_[5.0, 20.0] (9.0 ≤ (y5 - y4)))


    F16a                □_[0.0, 15.0] (0.0 < altitude)

    (NN 0.005 0.03)     □_[1.0, 18.0] (¬(abs((Pos - Ref)) ≤ (0.005 + (0.03 * abs(Ref)))) ⟹ ◇_[0.0, 2.0] □_[0.0, 1.0] (abs((Pos - Ref)) ≤ (0.005 + (0.03 * abs(Ref)))))


    SCa                 □_[30.0, 35.0] ((87.0 ≤ pressure) ∧ (pressure ≤ 87.5))

In addition, the following conjunctive benchmarks have been proposed:

    AT6abc              AT6a ∧ AT6b ∧ AT6c
    CCx                 □_[0.0, 50.0] (y2 - y1 > 7.5) ∧ □_[0.0, 50.0] (y3 - y2 > 7.5) ∧ □_[0.0, 50.0] (y4 - y3 > 7.5) ∧ □_[0.0, 50.0](y5 - y4 > 7.5)
    NNx                 ◇_[0.0, 1.0] (Pos > 3.2)  ∧  ◇_[1.0, 1.5] (□_[0, 0.5](1.75 < Pos < 2.25))  ∧  □_[2.0, 3.0] (1.825 < Pos < 2.175)