function [val,x] = supportFunc(hyp,d,varargin)
% supportFunc - computes support function of constrained hyperplane in a 
%    given direction
%
% Syntax:  
%    [val,x] = supportFunc(hyp,d)
%    [val,x] = supportFunc(hyp,d,type)
%
% Inputs:
%    hyp - conHyperplane object
%    d   - direction for which the bounds are calculated (vector of size
%          (n,1) )
%    type - upper or lower bound ('lower' or 'upper')
%
% Outputs:
%    val - bound of H in the specified direction
%    x   - point for which holds: dir'*x=val
%
% Example: 
%    hyp = conHyperplane(halfspace([1;1],0),[1 0;-1 0],[2;2]);
%    supportFunc(hyp,[-1;1]/sqrt(2))
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Victor Gassmann
% Written:      22-March-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% set default value
[type] = setDefaultValues({{'upper'}},varargin{:});

% check input arguments
inputArgsCheck({{hyp,'att',{'conHyperplane'},{''}};
                {d,'att',{'numeric'},{'column'}};
                {type,'str',{'lower','upper'}}});

% dimension mismatch
if ~isa(d,'double') || ~any(size(d)==1) || length(d)~=dim(hyp)
    throw(CORAerror('CORA:dimensionMismatch','obj1',hyp,'dim1',dim(hyp),...
        'obj2',d,'size2',size(d)));
end

% convert constrained hyperplane to polytope
hyp = mptPolytope(hyp);

% call support function for polytopes
if nargout<=1
    val = supportFunc(hyp,d,type);
elseif nargout==2
    [val,x] = supportFunc(hyp,d,type);
end

%------------- END OF CODE --------------