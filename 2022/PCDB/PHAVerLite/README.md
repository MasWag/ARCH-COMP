We provide a Docker file that:
 - sets up an Ubuntu machine
 - builds PPLite 0.7.1 from sources
 - builds PHAVerLite 0.4 from sources
 - downloads the model/config files for the ARCH-COMP 2022 PCDB tests
   (the same as the 2020 edition).

In order to run PHAVerLite on all tests,
the shell script measure_all should be executed.

More info on PHAVerLite can be obtained from its web page:
     https://github.com/ezaffanella/PHAVerLite


