import numpy as np

from staliro.specifications import RTAMTDense

from securitybenchmark import pendulum_model

step = 0.01
times = np.arange(0,15+step, 0.01)

signals = np.random.rand(len(times),2).T

stl_params = {
            'model': 'pendulum',
            'sim_end_time': 15,
            'eta': 0.223,
            'gamma': 0.9,
            'epsilon': 0.1,
            'alpha': 0.028
        }

timestamps, trajectories = pendulum_model(static = [], times = times, signals=signals, params = stl_params)


phi_1 = "not((always[0, {}] (prob_gt_over_eta <= {})) and (eventually[0, {}] prob_error_x_over_alpha >= {}))".format(
            stl_params['sim_end_time'],
            stl_params['epsilon'],
            stl_params['sim_end_time'],
            stl_params['gamma']
        )


specification_1 = RTAMTDense(phi_1, {"prob_gt_over_eta": 0, "prob_error_x_over_alpha": 1})
print(f"Phi 1: \t {phi_1}")
print(f"Robustness for Phi 1: {specification_1.evaluate(trajectories,timestamps)}")


phi_2 = "not(prob_gt_over_eta <= {} until (historically[0, 0.06] (prob_error_x_over_alpha >= {})))".format(
            stl_params['epsilon'],
            stl_params['gamma']
        )

specification_2 = RTAMTDense(phi_2, {"prob_gt_over_eta": 0, "prob_error_x_over_alpha": 1})

print(f"Phi 2: \t {phi_2}")
print(f"Robustness for Phi 2: {specification_2.evaluate(trajectories,timestamps)}")



